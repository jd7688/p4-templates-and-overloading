//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "DynamicArrayTemplate.h"
#include "LinkedList.hpp"
#include "NumberClass.h"

bool testArray();

int main ()
{
    testArray();
    
    return 0;
}

bool testArray()
{
    Array<float> array;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    std::cout << "Starting...\n";
    
    if (array.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        array.add (testArray[i]);
        
        if (array.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (array.get (i) != testArray[i])
        {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
    
    //removing first
    array.remove (0);
    if (array.size() != testArraySize - 1)
    {
        std::cout << "with size after removing item\n";
        return false;
    }
    
    for (int i = 0; i < array.size(); i++)
    {
        if (array.get(i) != testArray[i+1])
        {
            std::cout << "problems removing itemsa\n";
            return false;
        }
    }
    
    //removing last
    array.remove (array.size() - 1);
    if (array.size() != testArraySize - 2)
    {
        std::cout << "with size after removing item\n";
        return false;
    }
    
    for (int i = 0; i < array.size(); i++)
    {
        if (array.get(i) != testArray[i + 1])
        {
            std::cout << "problems removing items1\n";
            return false;
        }
    }
    
    //remove second item
    array.remove (1);
    if (array.size() != testArraySize - 3)
    {
        std::cout << "with size after removing item2\n";
        return false;
    }
    
    if (array.get (0) != testArray[1])
    {
        std::cout << "problems removing items3\n";
        return false;
    }
    
    if (array.get (1) != testArray[3])
    {
        std::cout << "problems removing items4\n";
        return false;
    }
    
    if (array.get (2) != testArray[4])
    {
        std::cout << "problems removing items5\n";
        return false;
    }
    
    std::cout << "\nNo Errors Detected.\n";
    
    return true;
}