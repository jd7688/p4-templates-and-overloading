//
//  DynamicArrayTemplate.h
//  CommandLineTool
//
//  Created by Joseph D'Souza on 13/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef DynamicArrayTemplate_h
#define DynamicArrayTemplate_h

template<class Turkey>
class Array
{
public:
    //create dynamic memory.
    Array(){
        aPointer = new Turkey[0];
        
        iAsize = 0;
    }
    
    //delete dynamic memory.
    ~Array(){
        delete[] aPointer;
        aPointer = nullptr;
        
    }
    
    //add new value.
    void add (Turkey itemValue){
        
        //memcpy more efficient.
        
        //create temporary pointer and new array.
        Turkey* aPointerTemp = nullptr;
        aPointerTemp = new Turkey[iAsize + 1];
        
        //copy old array values to new array.
        for(int i = 0; i <= iAsize; i++)
        {
            aPointerTemp[i] = aPointer[i];
        }
        
        //increment array index, input new value to array.
        iAsize++;
        aPointerTemp[iAsize -1 ] = itemValue;
        
        //delete old array and assign new array to class pointer(for the array).
        delete[] aPointer;
        aPointer = aPointerTemp;
        
    }
    
    //remove given value (in this case index).
    void remove (Turkey index){
        
        //memcpy more efficient.
        
        //create temporary pointer and new array.
        int i[2] = {0,0};
        Turkey* aPointerTemp = nullptr;
        aPointerTemp = new Turkey[iAsize -1];
        
        //copy old array values to new array. Skipping unwanted value.
        for(i[0] = 0; i[0] <= iAsize; i[0]++, i[1]++)
        {
            if(i[0] != index){
                aPointerTemp[i[0]] = aPointer[i[1]];
            }
            else{
                i[1]++;
                aPointerTemp[i[0]] = aPointer[i[1]];
            }
            
        }
        
        //decrement array index.
        iAsize--;
        
        //delete old array and assign new array to class pointer(for the array).
        delete[] aPointer;
        aPointer = aPointerTemp;
        
    }
    
    Turkey get (int index){
        
        //return requested array value.
        return aPointer[index];
    }
    
    Turkey size(){
        
        //return array index value.
        return iAsize;
        
        return 0;
    }
    
private:
    int iAsize;
    Turkey* aPointer = nullptr;
};

#endif /* DynamicArrayTemplate_h */
