//
//  NumberClass.h
//  CommandLineTool
//
//  Created by Joseph D'Souza on 13/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef NumberClass_h
#define NumberClass_h

template <class Type>
class Number
{
public:
    Type get() const
    {
        std::cout << "yes value" << std::endl;
        return value;
    }
    
    void set(Type newValue)
    {
        value = newValue;
    }
    
private:
    Type value;
};


#endif /* NumberClass_h */
